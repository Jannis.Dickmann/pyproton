import datetime
import logging
import colorlog

from proton.util.exceptions import ConfigError

# This global variable is a little ugly, but it works for now. The idea is to
# call get_logger with a filename argument the first time, and the module will
# remember this in FILENAME for successive calls to get_logger. By doing this,
# we can create a new logger each time this is called, but have each logger
# direct output to the console and to the same file. Additionally, each new
# logger can have a different log level, so we can configure log levels on a
# per-module basis.
FILENAME = None


def time_with_ms(record, datefmt=None):
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')


def time_with_s(record, datefmt=None):
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def get_logger(name, log_level=None, filename=None):
    """
    Get the standard logger, setting it up as necessary.

    Args:
        name (str): Name for the logger. Will be shown in log messages.

    Kwargs:
        log_level (str or numeric level): Logging level. Default is logging.INFO.
        filename (str): If included, sets up an additional FileHandler to
                        direct output to file.
    """
    # The user might want to pass in a numeric log level, so we should allow this:
    try:
        log_level = int(log_level)
    except (ValueError, TypeError):
        pass

    try:
        # Accept lowercase log levels like 'debug'
        log_level = log_level.upper()
    except AttributeError:
        # It isn't a string log level, which is fine
        pass

    logger = logging.getLogger(name)

    log_format = '%(asctime)s %(bold)s[%(name)s]%(reset)s %(log_color)s%(levelname)s - %(message)s'
    log_colors = {
        'DEBUG':    'bold',
        'INFO':     'cyan',
        'WARNING':  'yellow',
        'ERROR':    'red',
        'CRITICAL': 'bg_red',
    }
    if log_level is not None:
       logger.setLevel(log_level)
    else:
       logger.setLevel(logging.INFO)

    formatter = colorlog.ColoredFormatter(log_format, log_colors=log_colors)
    formatter.formatTime = time_with_s
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    if log_level is not None:
        handler.setLevel(log_level)
    else:
        handler.setLevel(logging.INFO)

    logger.addHandler(handler)

    logger.propagate = False

    if filename:
        global FILENAME
        FILENAME = filename

    if FILENAME:
        log_format = '%(asctime)s [%(name)s] %(levelname)s - %(message)s'
        handler = logging.FileHandler(FILENAME)
        handler.setLevel(logging.INFO)  # always log verbose to file
        formatter = logging.Formatter(log_format)
        formatter.formatTime = time_with_ms
        handler.setFormatter(formatter)
        logger.addHandler(handler)    

    return logger


# Handle warnings with the logger:
logging.captureWarnings(True)
_ = get_logger('py.warnings')


def validate_log_level(level):
    """
    Checks a log level parameter for compatibility with the logging module.

    Returns a cleaned up version of the original level (e.g., uppercased if necessary).

    Raises ConfigError if the level is invalid.
    """
    valid = True
    try:
        level = level.upper()
    except AttributeError:
        # Probably a numeric level
        try:
            level = int(level)
        except ValueError:
            valid = False
        else:
            if not 0 <= level <= 50:
                valid = False
    else:
        if level not in ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'NOTSET']:
            valid = False

    if not valid:
        raise ConfigError('Invalid log level: %s', level)

    return level
    
def get_filename():
    global FILENAME
    return FILENAME
