"""
proton.util.createrun
"""

import json
import os
from proton.util.validation import *
from proton.util import *
from pathlib import Path


class CreateRun:

    def __init__(self, config, system_config, log_level):
        self._config = config
        self._system_config = system_config
        self.log_level = validate_log_level(log_level)
        self.logger = get_logger('proton.createrun', self.log_level)
        self.logger.info('Creating new run configuration file.')

        self._config['scan']['input_dir'] = os.getcwd()

        self.logger.info('Please enter the following values. Leave your answer empty '
                         '(press enter) to accept default values.')
        self._config['scan']['dataset'] = validatedvariableinput(
            'dataset', self._config['scan']['dataset'], str, isvaliddataset
        )
        self._config['scan']['project'] = variableinput(
            'project', self._config['scan']['project']
        )
        self._config['scan']['phantom'] = variableinput(
            'phantom', self._config['scan']['phantom']
        )
        self._config['scan']['num_projections'] = validatedvariableinput(
            'num_projections', self._config['scan']['num_projections'], int, ispositiveint
        )
        if self._system_config['system']['integer_angles']:
            self._config['scan']['angular_step'] = validatedvariableinput(
                'angular_step', self._config['scan']['angular_step'], int, ispositiveint
            )
        else:
            self._config['scan']['angular_step'] = validatedvariableinput(
                'angular_step', self._config['scan']['angular_step'], float, ispositivenumber
            )

    def write(self):
        filename = '{}-{}-{}.json'.format(
            self._config['scan']['project'],
            self._config['scan']['phantom'],
            self._config['scan']['dataset']
        )
        # Create file in parent directory due to typical structure of simulations
        directory = Path(self._config['scan']['input_dir'])
        os.makedirs(directory.parent, exist_ok=True)
        file = os.path.join(directory.parent, filename)
        if os.path.isfile(file):
            write_config = parseynanswer(
                'A configuration file already exists in "{}". Do you want to overwrite?'.format(file)
            )
        else:
            write_config = True

        if write_config:
            self.logger.info('Writing new run configuration file.')
            with open(file, 'w') as outfile:
                json.dump(self._config, outfile, indent=4, sort_keys=False)
        else:
            self.logger.info('User cancelled writing of configuration file.')
