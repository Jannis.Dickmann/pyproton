"""
proton.util.config

Parsing/validation tools for the config file.
"""
import json
import sys

from proton.util.exceptions import ConfigError
from proton.util.logger import get_logger

logger = get_logger(__name__)

__all__ = [
    'parse_config',
    'validate_config',
    'override_log_level',
]


def parse_config(filename, type):
    logger.debug('Opening config file: %s', filename)
    try:
        with open(filename) as f:
            data = json.load(f)
    except FileNotFoundError:
        msg = 'The {} configuration file <{}> does not exist.'.format(type, filename)
        if type == 'run':
            msg += ' Please create a run configuration file by invoking <pyproton.py --create> within your data ' \
                    + 'directory.'
        elif type == 'system':
            msg += ' Please create a system configuration file by copying and modifying <system-config.sample.json> ' \
                    + 'to <system-config.json> within the installation directory of pyproton.py.'
        logger.error(msg)
        sys.exit(1)
    return data


def validate_config(data):
    """
    Raises ConfigError if any problem is found with the config data.
    Warnings will be logged, but will not raise an exception.

    :param data: parsed config dict.
    """
    check_for_required_keys(data)
    check_version(data)


def check_for_required_keys(data):
    if 'system_version' not in data['system']:
        raise ConfigError('File is missing version at root level')
    # TODO: add more required keys


def check_version(data):
    current_version = 1
    if data['system']['system_version'] != current_version:
        raise ConfigError('Unsupported system config version: %r' % data['system']['system_version'])


def override_log_level(data, log_level):
    """
    Recursively overrides all of the log_level settings in the config dict.
    Will be modified in place.

    :param data: parsed config dict.
    :param log_level: new log level.
    """
    if isinstance(data, dict):
        for key in data.keys():
            if key == 'log_level':
                data[key] = log_level
            else:
                override_log_level(data[key], log_level)
    elif isinstance(data, list):
        for d in data:
            override_log_level(d, log_level)
