"""
proton.workers.bins
Performs binning resulting in distance driven projections
"""

from proton.workers import BaseWorker
from proton.util.exceptions import ConfigError
from proton.util.validation import *


class Bins(BaseWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'isocenter': 0,
        'radius': 75,
        'mlptype': 'schulte',
        'origin': 'centered',
        'spacing': [1, 1, 1],
        'dimension': [200, 50, 200],
        'source': 0,
        'fill': True,
        'iterate': False,
        'iterate_interval': [1, 1, 1],
        'particle': 'proton'
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'bins', config, master, new_defaults=new_defaults)

        iteration_list = [0]
        if self.iterate:
            self.logger.warning('Worker is iterating.')
            iteration_list = range(
                self.iterate_interval[0],
                self.iterate_interval[1] + self.iterate_interval[2],
                self.iterate_interval[2]
            )

        # accumulate command queue
        for iter in iteration_list:
            for idx_angle in range(0, self.num_projections):
                command = []

                if self.iterate:
                    input_cut = 'cuts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_bins = 'bins{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_counts = 'counts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_var = 'vars{:05d}_{:04d}.mha'.format(iter, idx_angle)
                else:
                    input_cut = 'cuts{:04d}.mha'.format(idx_angle)
                    output_bins = 'bins{:04d}.mha'.format(idx_angle)
                    output_counts = 'counts{:04d}.mha'.format(idx_angle)
                    output_var = 'vars{:04d}.mha'.format(idx_angle)

                command.append('{}/pctbinning'.format(self.binary_dir))
                command.append('-i')
                command.append('{}/{}'.format(self.input_dir, input_cut))
                command.append('-o')
                command.append('{}/{}'.format(self.output_dir, output_bins))
                command.append('--spacing')
                command.append('{},{},{}'.format(self.spacing[0], self.spacing[1], self.spacing[2]))
                command.append('--dimension')
                command.append('{},{},{}'.format(self.dimension[0], self.dimension[1], self.dimension[2]))
                if not self.origin == 'centered':
                    command.append('--origin')
                    command.append('{},{},{}'.format(self.origin[0], self.origin[1], self.origin[2]))
                command.append('--source')
                command.append('{}'.format(self.source))
                command.append('--quadricIn')
                command.append('1,0,1,0,0,0,0,0,0,-{}'.format(self.radius*self.radius))
                command.append('--mlptype')
                command.append('{}'.format(self.mlptype))
                command.append('--count')
                command.append('{}/{}'.format(self.output_dir, output_counts))
                if self.particle != 'proton':
                    command.append('--particle')
                    command.append(self.particle)

                if self.fill:
                    command.append('--fill')

                if self.variance_reco:
                    command.append('--variance')
                    command.append('{}/{}'.format(self.output_dir, output_var))
                    if self.fill:
                        command.append('--fillvariance')

                self.command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.input_dir = self._get_output_dir('pbcorrection') if 'pbcorrection' in self.enabled_workers \
                                                              else self._get_output_dir('cuts')
        self.output_dir = self._get_output_dir()

        self.isocenter = self._get_config_value(self.name, 'isocenter')
        self.radius = self._get_config_value(self.name, 'radius')
        self.mlptype = self._get_config_value(self.name, 'mlptype')
        self.origin = self._get_config_value(self.name, 'origin')
        self.spacing = self._get_config_value(self.name, 'spacing')
        self.dimension = self._get_config_value(self.name, 'dimension')
        self.source = self._get_config_value(self.name, 'source')
        self.fill = self._get_config_value(self.name, 'fill')
        self.iterate = self._get_config_value(self.name, 'iterate')
        self.iterate_interval = self._get_config_value(self.name, 'iterate_interval')
        self.particle = self._get_config_value(self.name, 'particle')

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert isnumber(self.isocenter)
        assert ispositivenumber(self.radius)
        assert self.mlptype == 'schulte' or self.mlptype == 'polynomial'
        assert self.origin == 'centered' or isnumericlist(self.origin, minlength=3)
        assert isnumericlist(self.spacing, minlength=3)
        assert isnumericlist(self.dimension, minlength=3)
        assert isnumber(self.source)
        assert isbool(self.fill)
        assert isbool(self.iterate)
        assert isnumericlist(self.iterate_interval, minlength=3)
        for i in range(3):
            assert isint(self.iterate_interval[i])
        assert self.particle in ('proton', 'alpha')
