from proton.configurable import Configurable
import sys, os, time, json, stat
from subprocess import Popen, list2cmdline, DEVNULL, check_output
from proton.util import get_logger, validate_log_level
from proton.util.exceptions import ConfigError
from proton.util.validation import *


class BaseWorker(Configurable):
    """
    Basic worker without functionality, that can process batches of tasks
    in parallel or sequentially.
    """

    DEFAULTS = {
        'enabled': False,
        'multithreaded_global': True,
        'ingeter_angles': True
    }

    def __init__(self, name, config, master, new_defaults=None):
        self.name = name
        Configurable.__init__(self, config, new_defaults)
        self.master = master
        self.pre_command_queue = []
        self.command_queue = []
        self.post_command_queue = []
        self.multithreaded = True
        self.jobs_running = 0
        self.total_tasks = 0
        self.progressbar_state = 0
        self.max_jobs_parallel = 9999
        self.allowed_for_cluster = True

        self.logger = get_logger('proton.' + name, self.log_level)

    def _initialized(self):
        if len(self.command_queue):
            self.logger.info('Submitted a queue of %s commands.', len(self.command_queue))
            self.logger.debug('First command is <%s>.', list2cmdline(self.command_queue[0]))
        else:
            self.logger.info('Queue of commands is empty.')

    def run(self, verbose=False):
        """
        Execute commands in parallel in multiple processes
        """
        if not self.command_queue:
            self.logger.info('Command queue is empty.')
            return

        self.total_tasks = len(self.command_queue)
        time_started = time.time()

        if self.multithreaded and self.multithreaded_global:
            max_task = self._cpu_count()
        else:
            max_task = 1

        if self.machine == 'cluster' and not self.allowed_for_cluster:
            self.logger.info('Requested machine is cluster, but this worker cannot run on the cluster. Running '
                             'locally instead.')

        if len(self.pre_command_queue) > 0:
            if self.machine == 'local' or not self.allowed_for_cluster:
                self.logger.info('Working on pre command queue ({} commands).'.format(len(self.pre_command_queue)))
                self.run_queue_blocking(self.pre_command_queue, verbose)  # single-threaded
            elif self.machine == 'cluster':
                self.logger.info(
                    'Submitting pre command queue to cluster ({} commands).'.format(len(self.pre_command_queue)))
                self.cluster_queue_blocking(self.pre_command_queue, 'pre')  # single-threaded

        if self.machine == 'local' or not self.allowed_for_cluster:
            self.logger.info('Started to run %s commands.', self.total_tasks)
            self.run_queue(self.command_queue, max_task, True, verbose)  # potentially multi-threaded
        elif self.machine == 'cluster':
            self.logger.info('Submitting %s commands to cluster.', self.total_tasks)
            if self.multithreaded:
                self.cluster_queue(self.command_queue, 'main')
            else:
                self.cluster_queue_blocking(self.command_queue, 'main')

        if len(self.post_command_queue) > 0:
            if self.machine == 'local' or not self.allowed_for_cluster:
                self.logger.info('Working on post command queue ({} commands).'.format(len(self.post_command_queue)))
                self.run_queue_blocking(self.post_command_queue, verbose)  # single-threaded
            elif self.machine == 'cluster':
                self.logger.info('Submitting post command queue ({} commands).'.format(len(self.post_command_queue)))
                self.cluster_queue_blocking(self.post_command_queue, 'post')  # single-threaded

        time_elapsed = time.time() - time_started
        time_elapsed_str = '{0:.1f} min'.format(time_elapsed/60)
        self.logger.info('Finished execution of all commands. Time elapsed is %s.', time_elapsed_str)

    def run_queue(self, queue, max_task, stats=False, verbose=False):
        def done(p):
            return p.poll() is not None

        def success(p):
            return p.returncode == 0

        def fail():
            sys.exit(1)

        last_update_time = 0
        processes = []
        while True:
            while queue and len(processes) < max_task:
                task = queue.pop(0)
                if verbose:
                    print('Executing command <{}>.'.format(list2cmdline(task)))
                    processes.append(Popen(task))
                else:
                    processes.append(Popen(task, stdout=DEVNULL))

            for p in processes:
                if done(p):
                    if success(p):
                        processes.remove(p)
                    else:
                        fail()
            if stats:
                self.jobs_running = len(processes)
                current_time = time.time()
                if current_time - last_update_time > self.stats_interval:
                    self._update_stats()
                    last_update_time = current_time

            if not processes and not queue:
                self.jobs_running = 0
                break
            else:
                time.sleep(0.05)

        if stats:
            self._update_stats(True)

    @staticmethod
    def run_queue_blocking(queue, verbose=False):
        while queue:
            task = queue.pop(0)

            if verbose:
                print('Executing command <{}>.'.format(list2cmdline(task)))
                returncode = Popen(task).wait()
            else:
                returncode = Popen(task, stdout=DEVNULL).wait()

            if not returncode == 0:
                sys.exit(returncode)

    def cluster_queue(self, queue, identifier):
        idx = 0
        while queue:
            task = list2cmdline(queue.pop(0))
            fname = os.path.join(self.cluster_dir,
                                 '{}-{}-{}-{}.sh'.format(self.name, self.project_name, identifier, idx))

            with open(fname, 'w') as file:
                file.write('#!/bin/bash\n')
                file.write(task + '\n')

            # make executable
            os.chmod(fname, 0o777)

            idx += 1
        idx -= 1

        self._schedule_cluster(identifier, idx, max_jobs=self.max_jobs_parallel)

    def cluster_queue_blocking(self, queue, identifier):
        idx = 0
        fname = os.path.join(self.cluster_dir, '{}-{}-{}-{}.sh'.format(self.name, self.project_name, identifier, idx))
        with open(fname, 'w') as file:
            file.write('#!/bin/bash\n')
            while queue:
                task = list2cmdline(queue.pop(0))
                file.write(task + '\n')

        # make executable
        os.chmod(fname, 0o777)

        self._schedule_cluster(identifier, idx, max_jobs=self.max_jobs_parallel)

    def _schedule_cluster(self, identifier, count, max_jobs=None):
        if max_jobs is None:
            max_jobs = 9999
        shelldir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'shell'))

        command = []
        command.append(os.path.join(shelldir, 'schedule.sh'))
        command.append(shelldir)
        command.append('{:d}'.format(count))
        command.append(self._config['system']['cluster_partition'])
        command.append('4G')
        command.append(self.output_dir)
        command.append('{}-{}-{}'.format(self.name, self.project_name, identifier))
        command.append('{}'.format(self.master.last_job_id))
        command.append('END,FAIL' if self.name == 'reco' else 'FAIL')
        command.append('{:d}'.format(max_jobs))

        try:
            out = check_output(command).decode('ascii')
            self.master.last_job_id = int(out.split(' ')[-1])
            self.logger.info('Submitted job with ID {}.'.format(self.master.last_job_id))
        except ValueError:
            self.logger.exception("Unexepcted return value from schedule.sh. Cannot update job id of last job.")
            self.logger.info('Return value was: {}'.format(out))
            raise

    @staticmethod
    def _cpu_count():
        """
        Counts number of CPUs in system
        :return: number of CPUs available
        """
        num = 1
        if sys.platform == 'win32':
            try:
                num = int(os.environ['NUMBER_OF_PROCESSORS'])
            except (ValueError, KeyError):
                pass
        elif sys.platform == 'darwin':
            try:
                num = int(os.popen('sysctl -n hw.ncpu').read())
            except ValueError:
                pass
        else:
            try:
                num = os.sysconf('SC_NPROCESSORS_ONLN')
            except (ValueError, OSError, AttributeError):
                pass

        return num

    def _update_stats(self, last=False):
        if not self.master.verbose:
            self._print_progressbar(self.total_tasks-len(self.command_queue), self.total_tasks, self.jobs_running, suffix=self.name)
            if last:
                sys.stdout.write('\n')
                sys.stdout.flush()

    def _print_progressbar(self, iteration, total, running=0, prefix='', suffix='', decimals=1, length=36, fill='=', fillrunning='*', empty='-'):
        """
        Call in a loop to create terminal progress bar
        adapted from https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            running     - Optional  : iterations currently in progress (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        if self.progressbar_state == 1:
            fillrunning = empty
            self.progressbar_state = 0
        else:
            self.progressbar_state = 1
        percent = ("{0:3." + str(decimals) + "f}").format(100 * ((iteration-running) / float(total))).rjust(4+decimals)
        filled_length = int(length * (iteration-running) // total)
        running_length = int(length * running // total)
        bar = fill * filled_length + fillrunning * running_length + empty * (length - filled_length - running_length)
        end = ' ' * 10
        sys.stdout.write('%s [%s] %s%% %s (%s/%s)%s\r' % (prefix, bar, percent, suffix, iteration-running, total, end))
        sys.stdout.flush()

    def _parse_config(self):
        # from worker config
        self.enabled = self._get_config_value(self.name, 'enabled')
        self.log_level = self._get_config_value(self.name, 'log_level')
        # from scan config
        self.energy = self._get_config_value('scan', 'energy')
        self.dataset = self._get_config_value('scan', 'dataset')
        self.project = self._get_config_value('scan', 'project')
        self.project_name = self._get_config_value('scan', 'project_name')
        self.phantom = self._get_config_value('scan', 'phantom')
        self.input_dir = self._get_config_value('scan', 'input_dir')
        self.num_projections = self._get_config_value('scan', 'num_projections')
        self.angular_step = self._get_config_value('scan', 'angular_step')
        self.variance_reco = self._get_config_value('scan', 'variance_reco')
        self.enabled_workers = self._get_config_value('scan', 'enabled_workers')
        # from system config
        self.binary_dir = self._get_config_value('system', 'binary_dir')
        self.topoutput_dir = self._get_config_value('system', 'output_dir')
        self.output_dir = self.topoutput_dir
        self.output_location = self._get_config_value('system', 'output_location')
        self.output_dir_server = self._get_config_value('system', 'output_dir_server')
        self.output_dir_local = self._get_config_value('system', 'output_dir_local')
        self.rtk_dir = self._get_config_value('system', 'rtk_dir')
        self.preprocessing_dir = self._get_config_value('system', 'preprocessing_dir')
        self.readconvert_dir = self._get_config_value('system', 'readconvert_dir')
        self.fmpct_dir = self._get_config_value('system', 'fmpct_dir')
        self.calibration_dir = self._get_config_value('system', 'calibration_dir')
        self.stats_interval = self._get_config_value('system', 'stats_interval')
        self.multithreaded_global = self._get_config_value('system', 'multithreaded_global')
        self.machine = self._get_config_value('system', 'machine')
        self.integer_angles = self._get_config_value('system', 'integer_angles')

    def _validate_config(self):
        assert isbool(self.enabled), 'Invalid enabled status'
        self.log_level = validate_log_level(self.log_level)

        self._check_dirs()

        assert isbool(self.integer_angles)
        assert ispositivenumber(self.energy)
        assert isvaliddataset(self.dataset), 'Dataset must be either "realistic", "ideal" or "measurement"'
        assert ispositiveint(self.num_projections)
        if self.integer_angles:
            assert ispositiveint(self.angular_step, strict=True)
        else:
            assert ispositivenumber(self.angular_step, strict=True)
        assert isbool(self.variance_reco)
        assert ispositivenumber(self.stats_interval)
        assert self.output_location == 'server' or self.output_location == 'local'
        assert isbool(self.multithreaded_global) or self.multithreaded_global is None
        assert self.machine == 'local' or self.machine == 'cluster'

    def _get_worker_output_dir(self, worker=None, location=None):
        if worker is None:
            worker = self.name

        if location is None:
            output_dir = self.topoutput_dir
        elif location == 'server':
            output_dir = self.output_dir_server
        elif location == 'local':
            output_dir = self.output_dir_local
        else:
            raise ValueError('output_dir must either be server or local.')

        return os.path.join(output_dir, worker)

    def _get_output_dir(self, worker=None, location=None):
        return os.path.join(self._get_worker_output_dir(worker, location), self.project_name)

    def _check_dirs(self):
        # Check existence of input directory
        if not os.path.exists(self.input_dir):
            print(self.name)
            raise ConfigError('Input directory <{}> does not exist'.format(self.input_dir))
        # Create output directory
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        # cluster needs special output directory
        self.cluster_dir = os.path.join(self.output_dir, 'cluster')
        if not os.path.exists(self.cluster_dir) and self.output_dir != self.topoutput_dir:
            os.makedirs(self.cluster_dir)
        # Check existence of binary directory
        if not os.path.exists(self.binary_dir):
            raise ConfigError('Binary directory <{}> does not exist.'.format(self.binary_dir))
        # Check binaries
        if not os.path.isfile(os.path.join(self.binary_dir, 'pctpairprotonsLomaLinda')) or \
                not os.path.isfile(os.path.join(self.binary_dir, 'pctpairprotonsLMU_IMPCT')) or \
                not os.path.isfile(os.path.join(self.binary_dir, 'pctpaircuts')) or \
                not os.path.isfile(os.path.join(self.binary_dir, 'pctbinning')) or \
                not os.path.isfile(os.path.join(self.binary_dir, 'pctfdk')):
            raise ConfigError('Binary directory <{}> does not contain required binaries.'.format(self.binary_dir))
        # Check existence of RTK directory
        if not os.path.exists(self.rtk_dir):
            raise ConfigError('RTK directory <{}> does not exist.'.format(self.rtk_dir))
        # Check RTK binaries
        if not os.path.isfile(os.path.join(self.rtk_dir, 'rtksimulatedgeometry')) or \
                not os.path.isfile(os.path.join(self.rtk_dir, 'rtkfieldofview')):
            raise ConfigError('RTK binary directory <{}> does not contain required binaries.'.format(self.rtk_dir))
        # Check existence of FMpCT directory
        if not os.path.exists(self.fmpct_dir):
            raise ConfigError('FMpCT directory <{}> does not exist.'.format(self.fmpct_dir))
        # Check FMpCT binaries
        if not os.path.isfile(os.path.join(self.fmpct_dir, 'fmpctPencilBeamWEPLCorrector')):
            raise ConfigError('FMpCT binary directory <{}> does not contain required binaries.'.format(self.fmpct_dir))
        # Check existence of preprocessing directory
        if not os.path.exists(self.preprocessing_dir):
            raise ConfigError('Preprocessing directory <{}> does not exist.'.format(self.preprocessing_dir))
        # Check preprocessing binaries
        if not os.path.isfile(os.path.join(self.preprocessing_dir, 'pCT_Preprocessing')):
            raise ConfigError('Preprocessing binary directory <{}> does not contain required binaries.'.format(self.preprocessing_dir))
        # Check existence of readconvert directory
        if not os.path.exists(self.readconvert_dir):
            raise ConfigError('Readconvert directory <{}> does not exist.'.format(self.readconvert_dir))
        # Check readconvert binaries
        if not os.path.isfile(os.path.join(self.readconvert_dir, 'ReadConvert')):
            raise ConfigError('Readconvert binary directory <{}> does not contain required binaries.'.format(self.readconvert_dir))
        # Check existence of calibration directory
        if not os.path.exists(self.calibration_dir):
            raise ConfigError('Calibration directory does not exist')

    def _write_config(self, subdir=None):
        filename = '{}.json'.format(self.project_name)
        if subdir is None:
            directory = self._get_output_dir()
        else:
            directory = os.path.join(self._get_output_dir(), subdir)
        os.makedirs(directory, exist_ok=True)
        file = os.path.join(directory, filename)
        with open(file, 'w') as outfile:
            json.dump(self._config, outfile, indent=4, sort_keys=False)


class SpawnWorker(BaseWorker):
    """
    This worker spawns processes and does not wait for them to end.
    """

    def run(self, verbose=False):
        """
        Spawns commands without waiting for them to finish
        """
        if not self.command_queue:
            self.logger.info('Command queue is empty.')
            return

        while self.command_queue:
            task = self.command_queue.pop(0)
            if verbose:
                print('Executing command <{}>.'.format(list2cmdline(task)))
                Popen(task, stdin=None, close_fds=True)
            else:
                Popen(task, stdin=None, stdout=DEVNULL, close_fds=True)
