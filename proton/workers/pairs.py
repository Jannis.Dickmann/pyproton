"""
proton.workers.pairs
Performs pairing of proton data
"""

import os
import sys
from proton.workers import BaseWorker
from proton.util.exceptions import ConfigError
from proton.util.validation import *
import uproot
import numpy as np


class Pairs(BaseWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'fluence_modulation': False,
        'fluence_modulation_roi': 0,
        'fluence_modulation_fluence': 0.5,
        'iterate': False,
        'iterate_interval': [1, 1, 1],
        'equalize_num_protons': False,
        'equalize_num_protons_factor': 1.,
        'particle': 'proton'
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'pairs', config, master, new_defaults=new_defaults)

        self.logger.info('Reading dataset type: %s.', self.dataset)

        if self.fluence_modulation:
            self.logger.info('Using fluence modulation with radius %s and fluence fraction %s.', self.roi, self.fluence)
        elif self.equalize_num_protons:
            out = 'Equalizing number of protons in every projection.'
            if self.equalize_num_protons_factor != 1.:
                out += ' Reducing by an additional factor of {:.3f}.'.format(self.equalize_num_protons_factor)
            self.logger.info(out)

        self._target_num_protons = 0

        iteration_list = [0]
        if self.iterate:
            self.logger.warning('Worker is iterating.')
            iteration_list = range(
                self.iterate_interval[0],
                self.iterate_interval[1] + self.iterate_interval[2],
                self.iterate_interval[2]
            )

        # accumulate command queue
        for iter in iteration_list:
            for idx_angle, projection_angle in enumerate(np.linspace(0, self.angular_step*(self.num_projections-1), num=self.num_projections)):
                command = []
                if self.dataset == 'realistic' or self.dataset == 'measurement':
                    if self.iterate:
                        phasespace_file_name = self.input_file_in + '{:05d}_{:03d}'.format(
                            iter, idx_angle) + self.input_file_ending
                    else:
                        phasespace_file_name = self.input_file_in + '{:03d}'.format(
                            idx_angle) + self.input_file_ending

                    command.append('{}/pctpairprotonsLomaLinda'.format(self.binary_dir))
                    command.append('-i')
                    command.append('{}/{}'.format(self.input_dir, phasespace_file_name))
                    command.append('-o')
                    if self.iterate:
                        command.append('{}/pairs{:05d}_.mha'.format(self.output_dir, iter))
                    else:
                        command.append('{}/pairs.mha'.format(self.output_dir))
                    command.append('--runID={}'.format(idx_angle))
                elif self.dataset == 'ideal':
                    if self.iterate:
                        if self.integer_angles:
                            phasespace_in_file_name = self.input_file_in + '{:05d}_{:03d}'.format(
                                iter, int(projection_angle)) + self.input_file_ending
                            phasespace_out_file_name = self.input_file_out + '{:05d}_{:03d}'.format(
                                iter, int(projection_angle)) + self.input_file_ending
                        else:
                            phasespace_in_file_name = self.input_file_in + '{:05d}_{:05.1f}'.format(
                                iter, projection_angle) + self.input_file_ending
                            phasespace_out_file_name = self.input_file_out + '{:05d}_{:05.1f}'.format(
                                iter, projection_angle) + self.input_file_ending
                    else:
                        if self.integer_angles:
                            phasespace_in_file_name = self.input_file_in + '{:03d}'.format(int(projection_angle)) \
                                                      + self.input_file_ending
                            phasespace_out_file_name = self.input_file_out + '{:03d}'.format(int(projection_angle)) \
                                                       + self.input_file_ending
                        else:
                            phasespace_in_file_name = self.input_file_in + '{:05.1f}'.format(projection_angle) \
                                                      + self.input_file_ending
                            phasespace_out_file_name = self.input_file_out + '{:05.1f}'.format(projection_angle) \
                                                       + self.input_file_ending


                    command.append('{}/pctpairprotonsLMU_IMPCT'.format(self.binary_dir))
                    command.append('-i')
                    command.append('{}/{}'.format(self.input_dir, phasespace_in_file_name))
                    command.append('-j')
                    command.append('{}/{}'.format(self.input_dir, phasespace_out_file_name))
                    command.append('-o')
                    if self.iterate:
                        command.append('{}/pairs{:05d}_.mha'.format(self.output_dir, iter))
                    else:
                        command.append('{}/pairs.mha'.format(self.output_dir))
                    command.append('--runID={}'.format(idx_angle))
                    if self.particle != 'proton':
                        command.append('--particle')
                        command.append(self.particle)
                else:
                    raise ConfigError('Invalid dataset option.')

                if self.fluence_modulation:
                    command.append('--fmpct')
                    command.append('--roiR')
                    command.append('{}'.format(self.roi))
                    command.append('--modF')
                    command.append('{}'.format(self.fluence))
                elif self.equalize_num_protons:
                    command.append('--fmpct')
                    command.append('--roiR')
                    command.append('0')
                    command.append('--modF')
                    command.append('{}'.format(self._get_equalize_factor(projection_angle, iter)))

                self.command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        if self.dataset == 'realistic' or self.dataset == 'measurement':
            self.input_dir = self._get_output_dir('preprocess')
        else:
            self.input_dir = self._get_config_value('scan', 'input_dir')

        self.output_dir = self._get_output_dir()

        if self.dataset == 'realistic' or self.dataset == 'measurement':
            self.input_file_in = 'projection_'
            self.input_file_out = ''
            self.input_file_ending = '.root'
        elif self.dataset == 'ideal':
            self.input_file_in = 'outputIdealPhaseSpaceIn_'
            self.input_file_out = 'outputIdealPhaseSpaceOut_'
            self.input_file_ending = '.root'

        self.fluence_modulation = self._get_config_value(self.name, 'fluence_modulation')
        self.roi = self._get_config_value(self.name, 'fluence_modulation_roi')
        self.fluence = self._get_config_value(self.name, 'fluence_modulation_fluence')
        self.iterate = self._get_config_value(self.name, 'iterate')
        self.iterate_interval = self._get_config_value(self.name, 'iterate_interval')
        self.equalize_num_protons = self._get_config_value(self.name, 'equalize_num_protons')
        self.equalize_num_protons_factor = self._get_config_value(self.name, 'equalize_num_protons_factor')
        self.particle = self._get_config_value(self.name, 'particle')

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert isbool(self.fluence_modulation)
        assert ispositivenumber(self.roi, strict=False)
        assert ispositivenumber(self.fluence, strict=False) and self.fluence <= 1
        assert isbool(self.iterate)
        assert isnumericlist(self.iterate_interval, minlength=3)
        for i in range(3):
            assert isint(self.iterate_interval[i])
        assert isbool(self.equalize_num_protons)
        assert ispositivenumber(self.equalize_num_protons_factor)
        assert 0. < self.equalize_num_protons_factor <= 1.
        if self.equalize_num_protons and self.fluence_modulation:
            raise ConfigError('In pairs, options "fluence_modulation" and "equalize_num_protons" are mutually '
                              'exclusive.')
        if self.equalize_num_protons and 'preprocess' in self.enabled_workers:
            raise ConfigError('The option "equalize_num_protons" requires the output of the preprocess worker to '
                              'be available upon start. Therefore, please first run only preprocess, then run pairs '
                              'and following workers.')
        assert self.particle in ('proton', 'alpha')

    def _get_equalize_factor(self, projection_angle, iter=0):
        if projection_angle == 0 or self._target_num_protons == 0:
            self._target_num_protons = self._get_target_num_protons(iter)
        num = self._get_num_protons(projection_angle, iter)
        factor = self.equalize_num_protons_factor*float(self._target_num_protons)/float(num)
        self.logger.debug('Correction factor for angle {:.1f} is {:.3f}.'.format(projection_angle, factor))
        return factor

    def _get_target_num_protons(self, iter=0):
        self.logger.info('Calculating target number of protons for equalizing option. This can take a while.')
        target_num_protons = sys.maxsize

        for projection_angle in range(0, self.angular_step*self.num_projections, self.angular_step):
            num = self._get_num_protons(projection_angle, iter)
            if num < target_num_protons:
                target_num_protons = num

        self.logger.info('Target number of protons is {:d}. This is the smallest number of protons encountered in any '
                         'of the given projections.'.format(target_num_protons))

        return target_num_protons

    def _get_num_protons(self, projection_angle, iter=0):
        treename = 'PhaseSpaceTree' if self.dataset == 'ideal' else 'recoENTRY'
        if self.iterate:
            fname = self.input_file_in + '{:05d}_{:03d}'.format(iter, projection_angle) + self.input_file_ending
        else:
            fname = self.input_file_in + '{:03d}'.format(projection_angle) + self.input_file_ending
        data = uproot.open('{}/{}'.format(self.input_dir, fname))[treename]
        self.logger.debug('Found {:d} protons in file {}.'.format(data.numentries, fname))
        return data.numentries
