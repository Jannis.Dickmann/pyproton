"""
proton.workers.view
Views the reconstruction volume using an external program
"""

from proton.workers import SpawnWorker
import os


class View(SpawnWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'executable': 'vv'
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        SpawnWorker.__init__(self, 'view', config, master, new_defaults=new_defaults)

        self.multithreaded = False

        # do not do anything if reconstruction was not performed
        if 'reco' not in self.enabled_workers:
            self.logger.info('Reconstruction was not enabled, cannot view anything.')
            return
        if self._get_config_value('reco', 'iterate'):
            self.logger.info('Reconstruction was iterating. Disabling view worker.')
            return

        recovolume = os.path.join(self.input_dir, 'volume_' + self.phantom + '.mha')

        command = []
        command.append(self.executable)
        command.append(recovolume)
        if self.variance_reco:
            varrecovolume = os.path.join(self.input_dir, 'variance_' + self.phantom + '.mha')
            command.append(varrecovolume)

        self.command_queue.append(command)

        self._initialized()

    def _parse_config(self):
        SpawnWorker._parse_config(self)

        self.executable = self._get_config_value(self.name, 'executable')

        if 'reco' in self.enabled_workers:
            self.input_dir = self._get_output_dir('reco', 'server')
            self.output_dir = self.topoutput_dir
