"""
proton.workers.copydata
Copies data from local storage to the server and cleans up local storage
"""

from proton.workers import BaseWorker
from proton.util.validation import *
import os


class CopyData(BaseWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'copy': ['preprocess', 'cuts', 'pbcorrection', 'bins'],
        'cleanup': True
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'copydata', config, master, new_defaults=new_defaults)

        self.multithreaded = False

        # do not do anything if data is already on server
        if self.output_location == 'server':
            self.logger.info('Data location is already server. Nothing to copy or remove.')
            return

        msg = 'Copying data from local to server.'
        if self.cleanup:
            msg += ' Removing local files.'
        self.logger.info(msg)

        dirs_in = []
        dirs_out = []
        rm_dirs = []

        # PREPROCESS
        if 'preprocess' in self.enabled_workers:
            rm_dirs.append(self._get_output_dir('preprocess', 'local'))
            if 'preprocess' in self.copy and (self.dataset == 'realistic' or self.dataset == 'measurement'):
                dirs_in.append(self._get_output_dir('preprocess', 'local'))
                dirs_out.append(self._get_output_dir('preprocess', 'server'))

        # PAIRS
        if 'pairs' in self.enabled_workers:
            rm_dirs.append(self._get_output_dir('pairs', 'local'))
            if 'pairs' in self.copy:
                dirs_in.append(self._get_output_dir('pairs', 'local'))
                dirs_out.append(self._get_output_dir('pairs', 'server'))

        # CUTS
        if 'cuts' in self.enabled_workers:
            rm_dirs.append(self._get_output_dir('cuts', 'local'))
            if 'cuts' in self.copy:
                dirs_in.append(self._get_output_dir('cuts', 'local'))
                dirs_out.append(self._get_output_dir('cuts', 'server'))

        # PBCORRECTION
        if 'pbcorrection' in self.enabled_workers:
            rm_dirs.append(self._get_output_dir('pbcorrection', 'local'))
            if 'pbcorrection' in self.copy:
                dirs_in.append(self._get_output_dir('pbcorrection', 'local'))
                dirs_out.append(self._get_output_dir('pbcorrection', 'server'))

        # BINS
        if 'bins' in self.enabled_workers:
            rm_dirs.append(self._get_output_dir('bins', 'local'))
            if 'bins' in self.copy:
                dirs_in.append(self._get_output_dir('bins', 'local'))
                dirs_out.append(self._get_output_dir('bins', 'server'))

        for i in range(len(dirs_in)):
            command = []
            command.append('mkdir')
            command.append('-p')
            command.append('{}/'.format(dirs_out[i]))
            self.command_queue.append(command)

            command = []
            command.append('rsync')
            command.append('-av')
            command.append('{}/'.format(dirs_in[i]))
            command.append('{}/'.format(dirs_out[i]))
            self.command_queue.append(command)

        for i in range(len(rm_dirs)):
            if self.cleanup:
                command = []
                command.append('rm')
                command.append('-rf')
                command.append('{}'.format(rm_dirs[i]))
                self.command_queue.append(command)

        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.cleanup = self._get_config_value(self.name, 'cleanup')
        self.copy = self._get_config_value(self.name, 'copy')

        # just defined for correct validation
        self.input_dir = self.topoutput_dir
        self.output_dir = self.topoutput_dir

        # get calibration from preprocess worker
        if self.dataset == 'realistic' or self.dataset == 'measurement':
            self.calibration = self._get_config_value('preprocess', 'calibration')
        else:
            self.calibration = None

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert isbool(self.cleanup)
        assert islist(self.copy)
