"""
proton.workers.cuts
Performs cuts on the proton data
"""

from proton.workers import BaseWorker
from proton.util.exceptions import ConfigError
from proton.util.validation import *


class Cuts(BaseWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'origin': 'centered',
        'spacing': [2, 2],
        'dimension': [100, 25],
        'source': 0,
        'sigma_angle': 3,
        'sigma_wepl': 3,
        'iterate': False,
        'iterate_interval': [1, 1, 1]
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'cuts', config, master, new_defaults=new_defaults)

        iteration_list = [0]
        if self.iterate:
            self.logger.warning('Worker is iterating.')
            iteration_list = range(
                self.iterate_interval[0],
                self.iterate_interval[1] + self.iterate_interval[2],
                self.iterate_interval[2]
            )

        # accumulate command queue
        for iter in iteration_list:
            for idx_angle in range(0, self.num_projections):
                command = []

                if self.iterate:
                    input_pair = 'pairs{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_cuts = 'cuts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_counts = 'counts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                else:
                    input_pair = 'pairs{:04d}.mha'.format(idx_angle)
                    output_cuts = 'cuts{:04d}.mha'.format(idx_angle)
                    output_counts = 'counts{:04d}.mha'.format(idx_angle)

                command.append('{}/pctpaircuts'.format(self.binary_dir))
                command.append('-i')
                command.append('{}/{}'.format(self.input_dir, input_pair))
                command.append('-o')
                command.append('{}/{}'.format(self.output_dir, output_cuts))
                command.append('--spacing')
                command.append('{},{}'.format(self.spacing[0], self.spacing[1]))
                command.append('--dimension')
                command.append('{},{}'.format(self.dimension[0], self.dimension[1]))
                if not self.origin == 'centered':
                    command.append('--origin')
                    command.append('{},{}'.format(self.origin[0], self.origin[1]))
                command.append('--source')
                command.append('{}'.format(self.source))
                command.append('--anglecut')
                command.append('{}'.format(self.sigma_angle))
                command.append('--energycut')
                command.append('{}'.format(self.sigma_wepl))
                command.append('--robust')
                command.append('--robustopt=0')
                command.append('--count')
                command.append('{}/{}'.format(self.output_dir, output_counts))

                self.command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.input_dir = self._get_output_dir('pairs')
        self.output_dir = self._get_output_dir()

        self.origin = self._get_config_value(self.name, 'origin')
        self.spacing = self._get_config_value(self.name, 'spacing')
        self.dimension = self._get_config_value(self.name, 'dimension')
        self.source = self._get_config_value(self.name, 'source')
        self.sigma_angle = self._get_config_value(self.name, 'sigma_angle')
        self.sigma_wepl = self._get_config_value(self.name, 'sigma_wepl')
        self.iterate = self._get_config_value(self.name, 'iterate')
        self.iterate_interval = self._get_config_value(self.name, 'iterate_interval')

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert self.origin == 'centered' or isnumericlist(self.origin, minlength=2)
        assert isnumericlist(self.spacing, minlength=2)
        assert isnumericlist(self.dimension, minlength=2)
        assert isnumber(self.source)
        assert ispositivenumber(self.sigma_angle, strict=False)
        assert ispositivenumber(self.sigma_wepl, strict=False)
        assert isbool(self.iterate)
        assert isnumericlist(self.iterate_interval, minlength=3)
        for i in range(3):
            assert isint(self.iterate_interval[i])
