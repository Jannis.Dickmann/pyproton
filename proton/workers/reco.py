"""
proton.workers.fdk
Performs distance driven filtered backprojection.
"""

from proton.workers import BaseWorker
from proton.util.validation import *


class Reco(BaseWorker):
    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'start_angle': 0,
        'sdd': 0,
        'sid': 10,
        'rotation': 360,
        'pad': 0,
        'hann': 0,
        'iterate': False,
        'iterate_interval': [1, 1, 1],
        'flip': False,
        'permute': False
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'reco', config, master, new_defaults=new_defaults)

        # Do not run commands in parallel
        self.multithreaded = False

        iteration_list = [0]
        if self.iterate:
            self.logger.warning('Worker is iterating.')
            iteration_list = range(
                self.iterate_interval[0],
                self.iterate_interval[1] + self.iterate_interval[2],
                self.iterate_interval[2]
            )

        # Calculate geometry
        command = []
        output_geometry = 'output_geometry.rtk'
        command.append('{}/rtksimulatedgeometry'.format(self.rtk_dir))
        command.append('-n')
        command.append('{}'.format(self.num_projections))
        command.append('-f')
        command.append('{}'.format(self.start_angle))
        command.append('-a')
        command.append('{}'.format(self.rotation))
        command.append('-o')
        command.append('{}/{}'.format(self.output_dir, output_geometry))
        command.append('--sdd')
        command.append('{}'.format(self.sdd))
        command.append('--sid')
        command.append('{}'.format(self.sid))
        command.append('--proj_iso_x')
        command.append('{}'.format(self.isocenter))
        self.command_queue.append(command)

        for iter in iteration_list:
            if self.iterate:
                binsregex = 'bins{:05d}_.*.mha'.format(iter)
                varsregex = 'vars{:05d}_.*.mha'.format(iter)
                output_unmasked = 'unmasked{:05d}.mha'.format(iter)
                output_volume = 'volume{:05d}_'.format(iter) + self.phantom + '.mha'
                output_variance_unmasked = 'variance_unmasked{:05d}.mha'.format(iter)
                output_variance_volume = 'variance{:05d}_'.format(iter) + self.phantom + '.mha'
            else:
                binsregex = 'bins.*.mha'
                varsregex = 'vars.*.mha'
                output_unmasked = 'unmasked.mha'
                output_volume = 'volume_' + self.phantom + '.mha'
                output_variance_unmasked = 'variance_unmasked.mha'
                output_variance_volume = 'variance_' + self.phantom + '.mha'

            # Perform reconstruction
            command = []
            command.append('{}/pctfdk'.format(self.binary_dir))
            command.append('--lowmem')
            command.append('--dimension')
            command.append('{},{},{}'.format(self.dimension[0], self.dimension[1], self.dimension[2]))
            command.append('--spacing')
            command.append('{},{},{}'.format(self.spacing[0], self.spacing[1], self.spacing[2]))
            if not self.origin == 'centered':
                command.append('--origin')
                command.append('{},{},{}'.format(self.origin[0], self.origin[1], self.origin[2]))
            command.append('--pad')
            command.append('{}'.format(self.pad))
            command.append('--hann')
            command.append('{}'.format(self.hann))
            command.append('-r')
            command.append(binsregex)
            command.append('--verbose')
            command.append('-g')
            command.append('{}/{}'.format(self.output_dir, output_geometry))
            command.append('-p')
            command.append('{}/.'.format(self.input_dir))
            command.append('-o')
            command.append('{}/{}'.format(self.output_dir, output_unmasked))
            self.command_queue.append(command)

            # Crop field of view
            command = []
            command.append('{}/rtkfieldofview'.format(self.rtk_dir))
            command.append('-p')
            command.append('{}'.format(self.input_dir))
            command.append('-r')
            command.append(binsregex)
            command.append('-g')
            command.append('{}/{}'.format(self.output_dir, output_geometry))
            command.append('--reconstruction')
            command.append('{}/{}'.format(self.output_dir, output_unmasked))
            command.append('--output')
            command.append('{}/{}'.format(self.output_dir, output_volume))
            self.command_queue.append(command)

            # Remove unmasked file
            command = []
            command.append('rm')
            command.append('-f')
            command.append('{}/{}'.format(self.output_dir, output_unmasked))
            self.command_queue.append(command)

            # Transpose output volume
            if (self.flip is not None and not isbool(self.flip)) or \
                    (self.permute is not None and not isbool(self.permute)):
                command = []
                command.append('{}/fmpctTranspose'.format(self.fmpct_dir))
                command.append('--input')
                command.append('{}/{}'.format(self.output_dir, output_volume))
                if self.flip is not None and not isbool(self.flip):
                    command.append('--flip')
                    flip = ''
                    for f in self.flip:
                        flip += '{:d},'.format(f)
                    flip = flip[:-1]
                    command.append(flip)
                if self.permute is not None and not isbool(self.permute):
                    command.append('--permute')
                    command.append('{:d},{:d},{:d}'.format(self.permute[0], self.permute[1], self.permute[2]))
                self.command_queue.append(command)

            if self.variance_reco:
                # Perform variance reconstruction
                command = []
                command.append('{}/pctfdk'.format(self.binary_dir))
                command.append('--lowmem')
                command.append('--dimension')
                command.append('{},{},{}'.format(self.dimension[0], self.dimension[1], self.dimension[2]))
                command.append('--spacing')
                command.append('{},{},{}'.format(self.spacing[0], self.spacing[1], self.spacing[2]))
                if not self.origin == 'centered':
                    command.append('--origin')
                    command.append('{},{},{}'.format(self.origin[0], self.origin[1], self.origin[2]))
                command.append('--pad')
                command.append('{}'.format(self.pad))
                command.append('--hann')
                command.append('{}'.format(self.hann))
                command.append('-r')
                command.append(varsregex)
                command.append('--verbose')
                command.append('-g')
                command.append('{}/{}'.format(self.output_dir, output_geometry))
                command.append('-p')
                command.append('{}/.'.format(self.input_dir))
                command.append('-o')
                command.append('{}/{}'.format(self.output_dir, output_variance_unmasked))
                command.append('--variance')
                self.command_queue.append(command)

                # Crop field of view
                command = []
                command.append('{}/rtkfieldofview'.format(self.rtk_dir))
                command.append('-p')
                command.append('{}'.format(self.input_dir))
                command.append('-r')
                command.append(varsregex)
                command.append('-g')
                command.append('{}/{}'.format(self.output_dir, output_geometry))
                command.append('--reconstruction')
                command.append('{}/{}'.format(self.output_dir, output_variance_unmasked))
                command.append('--output')
                command.append('{}/{}'.format(self.output_dir, output_variance_volume))
                self.command_queue.append(command)

                # Remove unmasked file
                command = []
                command.append('rm')
                command.append('-f')
                command.append('{}/{}'.format(self.output_dir, output_variance_unmasked))
                self.command_queue.append(command)

                # Transpose output volume
                if (self.flip is not None and not isbool(self.flip)) or \
                        (self.permute is not None and not isbool(self.permute)):
                    command = []
                    command.append('{}/fmpctTranspose'.format(self.fmpct_dir))
                    command.append('--input')
                    command.append('{}/{}'.format(self.output_dir, output_variance_volume))
                    if self.flip is not None and not isbool(self.flip):
                        command.append('--flip')
                        flip = ''
                        for f in self.flip:
                            flip += '{:d},'.format(f)
                        flip = flip[:-1]
                        command.append(flip)
                    if self.permute is not None and not isbool(self.permute):
                        command.append('--permute')
                        command.append('{:d},{:d},{:d}'.format(self.permute[0], self.permute[1], self.permute[2]))
                    self.command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.input_dir = self._get_output_dir('bins')
        self.output_dir = self._get_output_dir(self.name, 'server')  # reco always writes to the server

        self.start_angle = self._get_config_value(self.name, 'start_angle')
        self.sdd = self._get_config_value(self.name, 'sdd')
        self.sid = self._get_config_value(self.name, 'sid')
        self.rotation = self._get_config_value(self.name, 'rotation')
        self.pad = self._get_config_value(self.name, 'pad')
        self.hann = self._get_config_value(self.name, 'hann')
        self.origin = self._get_config_value(self.name, 'origin')
        self.spacing = self._get_config_value(self.name, 'spacing')
        self.dimension = self._get_config_value(self.name, 'dimension')
        self.isocenter = self._get_config_value(self.name, 'isocenter')
        self.iterate = self._get_config_value(self.name, 'iterate')
        self.iterate_interval = self._get_config_value(self.name, 'iterate_interval')
        self.flip = self._get_config_value(self.name, 'flip')
        self.permute = self._get_config_value(self.name, 'permute')
        if self.permute and self.permute[0] == 0 and self.permute[1] == 1 and self.permute[2] == 2:
            self.permute = False

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert isnumber(self.start_angle)
        assert ispositivenumber(self.sdd, strict=False)
        assert ispositivenumber(self.sid, strict=False)
        assert isint(self.rotation)
        assert self.origin == 'centered' or isnumericlist(self.origin, minlength=3)
        assert isnumericlist(self.spacing, minlength=3)
        assert isnumericlist(self.dimension, minlength=3)
        assert isnumber(self.isocenter)
        assert isbool(self.iterate)
        assert isnumericlist(self.iterate_interval, minlength=3)
        for i in range(3):
            assert isint(self.iterate_interval[i])
        assert isbool(self.flip) or isnumericlist(self.flip)
        if isnumericlist(self.flip):
            for i in range(len(self.flip)):
                assert isint(self.flip[i]) and 0 <= self.flip[i] <= 2
        assert isbool(self.permute) or isnumericlist(self.permute, minlength=3)
        if isnumericlist(self.permute, minlength=3):
            for i in range(3):
                assert isint(self.permute[i]) and 0 <= self.permute[i] <= 2
