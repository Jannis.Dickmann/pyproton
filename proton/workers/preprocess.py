"""
proton.workers.preprocess
Performs preprocessing of proton data
"""

import os
import numpy as np
from proton.workers import BaseWorker
from proton.util.exceptions import ConfigError
from proton.util.validation import *


class Preprocess(BaseWorker):

    DEFAULTS = {
        'enabled': True,
        'log_level': 'info',
        'data_filename': 'output_',
        'data_extension': 'bin',
        'calibration': None,
        'order': 'variable',
        'include': None,
        'monte_carlo': 'no',
        'radius': 110,
        'thresholds': [1, 1, 1, 1, 1],
        'pedrng' : [-500, -500, -500, -1500, -1500],
        'recalibrate': 'yes',
        'continuous_run': False,
        'max_num_events': 0,
        'fraction': 1.,
        'particle': 'H'
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'preprocess', config, master, new_defaults=new_defaults)

        self.max_jobs_parallel = int(self.max_jobs_parallel/8.)
        self.allowed_for_cluster = False

        # Make sure the respective preprocessing run has finished before readconvert is run on this data
        if self.num_projections <= 2*self._cpu_count():
            self.logger.info("Falling back to sequential mode to make sure preprocessing and ReadConvert are performed "
                             "in order.")
            self.multithreaded = False

        if self.dataset != 'realistic' and self.dataset != 'measurement':
            self.logger.info('Dataset is %s. Nothing to preprocess.', self.dataset)
            return

        self.logger.info('Preprocessing dataset type: %s.', self.dataset)
        assert isnotnone(self.calibration)

        if not self.continuous_run:
            # first perform preprocessing
            for idx_angle, projection_angle in enumerate(np.linspace(0, self.angular_step*(self.num_projections-1), num=self.num_projections)):
                command = []
                command.append('mkdir')
                command.append('-p')
                command.append('{}/tmp{}'.format(self.output_dir, idx_angle))
                self.pre_command_queue.append(command)

                command = []
                command.append('{}/pCT_Preprocessing'.format(self.preprocessing_dir))
                command.append('--ntuple=no')
                command.append('--debug=0')
                command.append('--tempFile=yes')
                command.append('--order={}'.format(self.order))
                if self.include is not None:
                    command.append('--include={}'.format(','.join(self.include)))
                command.append('-n')
                command.append('0')
                command.append('--time=0')
                command.append('--plot=1')
                if self.monte_carlo:
                    command.append('--MonteCarlo=yes')
                else:
                    command.append('--MonteCarlo=no')
                command.append('--user=no')
                command.append('--projection=0')  # force output file to be projection_000.bin
                command.append('--bins=1')
                command.append('--outputDir={}/tmp{}'.format(self.output_dir, idx_angle))
                command.append('-W')
                command.append('{}/{}/Wcalib.txt'.format(self.calibration_dir, self.calibration))
                command.append('-T')
                command.append('{}/{}/TVcorr.txt'.format(self.calibration_dir, self.calibration))
                command.append('--mindate=1970/1/1')
                command.append('--maxdate=2037/1/1')
                command.append('--minrun=1')
                command.append('--maxrun=99999999999')
                command.append('--log=NULL')
                command.append('--angle=0.')
                command.append('--size={}'.format(self.radius))
                for i in range(5):
                    command.append('--thr{}={}'.format(i, self.thresholds[i]))
                for i in range(5):
                    command.append('--pedrng{}={}'.format(i, self.pedrng[i]))
                command.append('--particle={}'.format(self.particle))
                command.append('--level=2')
                command.append('--calibrate=no')
                if self.recalibrate:
                    command.append('--recalibrate=yes')
                else:
                    command.append('--recalibrate=no')
                command.append('--threads=1')
                if self.max_num_events > 0:
                    command.append('--number={:d}'.format(self.max_num_events))
                if self.fraction != 1.:
                    command.append('--fraction={:f}'.format(self.fraction))
                command.append('--offset=0.')
                if self.dEEFilter:
                    command.append('--dEEFilter=yes')
                else:
                    command.append('--dEEFilter=no')
                if self.integer_angles:
                    command.append('{}/{}{:03d}{}'.format(
                        self.input_dir, self.data_filename, int(projection_angle), self.data_extension)
                    )
                else:
                    command.append('{}/{}{:05.1f}{}'.format(
                        self.input_dir, self.data_filename, projection_angle, self.data_extension)
                    )
                self.command_queue.append(command)

            # then perform all ReadConvert
            for idx_angle, projection_angle in enumerate(np.linspace(0, self.angular_step*(self.num_projections-1), num=self.num_projections)):
                command = []
                command.append('{}/ReadConvert'.format(self.readconvert_dir))
                command.append('1')  # there is just one projection in the temporary folder
                command.append('1')  # the angular step doesnt matter since it's just one projection
                command.append('{}/tmp{}'.format(self.output_dir, idx_angle))
                command.append('projection_')  # This is the filename of the output file
                command.append('1')  # This is a fluence modulation factor, which we don't need
                self.command_queue.append(command)

                command = []
                command.append('mv')
                command.append('{}/tmp{}/projection_000.root'.format(self.output_dir, idx_angle))
                command.append('{}/projection_{:03d}.root'.format(self.output_dir, idx_angle))
                self.post_command_queue.append(command)

                command = []
                command.append('rm')
                command.append('-r')
                command.append('{}/tmp{}'.format(self.output_dir, idx_angle))
                self.post_command_queue.append(command)
        else:
            command = []
            command.append('{}/pCT_Preprocessing'.format(self.preprocessing_dir))
            command.append('--ntuple=no')
            command.append('--debug=0')
            command.append('--tempFile=yes')
            command.append('--order={}'.format(self.order))
            if self.include is not None:
                command.append('--include={}'.format(','.join(self.include)))
            command.append('-n')
            command.append('0')
            command.append('--time=0')
            command.append('--plot=1')
            if self.monte_carlo:
                command.append('--MonteCarlo=yes')
            else:
                command.append('--MonteCarlo=no')
            command.append('--user=no')
            command.append('--bins={}'.format(self.num_projections))
            command.append('--outputDir={}'.format(self.output_dir))
            command.append('-W')
            command.append('{}/{}/Wcalib.txt'.format(self.calibration_dir, self.calibration))
            command.append('-T')
            command.append('{}/{}/TVcorr.txt'.format(self.calibration_dir, self.calibration))
            command.append('--mindate=1970/1/1')
            command.append('--maxdate=2037/1/1')
            command.append('--minrun=1')
            command.append('--maxrun=9999')
            command.append('--log=NULL')
            command.append('--angle=0.')
            command.append('--size={}'.format(self.radius))
            for i in range(5):
                command.append('--thr{}={}'.format(i, self.thresholds[i]))
            for i in range(5):
                command.append('--pedrng{}={}'.format(i, self.pedrng[i]))
            command.append('--particle={}'.format(self.particle))
            command.append('--level=2')
            command.append('--calibrate=no')
            if self.recalibrate:
                command.append('--recalibrate=yes')
            else:
                command.append('--recalibrate=no')
            command.append('--threads=1')
            if self.max_num_events > 0:
                command.append('--number={:d}'.format(self.max_num_events))
            if self.fraction != 1.:
                command.append('--fraction={:f}'.format(self.fraction))
            command.append('--offset=0.')
            if self.dEEFilter:
                command.append('--dEEFilter=yes')
            else:
                command.append('--dEEFilter=no')
            command.append('{}/{}{}'.format(
                self.input_dir, self.data_filename, self.data_extension)
            )

            self.command_queue.append(command)

            # add readconvert command
            command = []
            command.append('{}/ReadConvert'.format(self.readconvert_dir))
            command.append('{}'.format(self.num_projections))
            command.append('{}'.format(self.angular_step))
            command.append('{}'.format(self.output_dir))
            command.append('projection_')  # This is the filename of the output file
            command.append('1')  # This is a fluence modulation factor, which we don't need
            self.post_command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.input_dir = self._get_config_value('scan', 'input_dir')
        if self.dataset == 'realistic' or self.dataset == 'measurement':
            self.calibration = self._get_config_value(self.name, 'calibration')
            self.output_dir = self._get_output_dir()
        else:
            self.calibration = None
            self.output_dir = self._get_output_dir('pairs')

        self.data_filename = self._get_config_value(self.name, 'data_filename')
        self.data_extension = self._get_config_value(self.name, 'data_extension')
        self.order = self._get_config_value(self.name, 'order')
        self.include = self._get_config_value(self.name, 'include')
        self.monte_carlo = self._get_config_value(self.name, 'monte_carlo')
        self.radius = self._get_config_value(self.name, 'radius')
        self.thresholds = self._get_config_value(self.name, 'thresholds')
        self.pedrng = self._get_config_value(self.name, 'pedrng')
        self.recalibrate = self._get_config_value(self.name, 'recalibrate')
        self.continuous_run = self._get_config_value(self.name, 'continuous_run')
        self.dEEFilter = self._get_config_value(self.name, 'dEEFilter')
        self.max_num_events = self._get_config_value(self.name, 'max_num_events')
        self.fraction = self._get_config_value(self.name, 'fraction')
        self.particle = self._get_config_value(self.name, 'particle')

    def _validate_config(self):
        BaseWorker._validate_config(self)

        # Check existence of calibration files
        if self.dataset == 'realistic' or self.dataset == 'measurement':
            #print("Problematic statement")
            #print((os.path.join(self.calibration_dir, self.calibration, 'Wcalib.txt')))
            assert os.path.isfile(os.path.join(self.calibration_dir, self.calibration, 'Wcalib.txt'))
            assert os.path.isfile(os.path.join(self.calibration_dir, self.calibration, 'TVcorr.txt'))

        assert self.order == 'variable' or self.order == 'event'
        assert self.include is None or islist(self.include)
        assert isbool(self.monte_carlo)
        assert ispositivenumber(self.radius)
        assert isnumericlist(self.thresholds, minlength=5)
        for i in range(5):
            assert ispositivenumber(self.thresholds[i])
        assert isnumericlist(self.pedrng, minlength=5)
        for i in range(5):
            assert isnumber(self.pedrng[i])
        assert isbool(self.recalibrate)
        assert isbool(self.continuous_run)
        assert isbool(self.dEEFilter)
        assert isint(self.max_num_events)
        assert ispositivenumber(self.fraction)
        assert self.particle == 'H' or self.particle == 'He'
