"""
proton.workers.pbcorrection
Performs pencil beam correction given a per-pencil-beam correction function
"""

from proton.workers import BaseWorker
from proton.util.exceptions import ConfigError
from proton.util.validation import *
import os


class PBCorrection(BaseWorker):

    DEFAULTS = {
        'enabled': False,
        'log_level': 'info',
        'weplcorrection': None,
        'threshold': 100,
        'interval': 0.9,
        'beams': 273,
        'relax': True,
        'iterate': False,
        'iterate_interval': [1, 1, 1],
        'skip_execution': False,
        'cut': None
    }

    def __init__(self, config, master, new_defaults=None):
        # Invoke parent constructor
        BaseWorker.__init__(self, 'pbcorrection', config, master, new_defaults=new_defaults)

        iteration_list = [0]
        if self.iterate:
            self.logger.warning('Worker is iterating.')
            iteration_list = range(
                self.iterate_interval[0],
                self.iterate_interval[1] + self.iterate_interval[2],
                self.iterate_interval[2]
            )

        if self.skip_execution:
            self.logger.info('Skipping execution of this worker due to request from run configuration.')
            iteration_list = []

        # accumulate command queue
        for iter in iteration_list:
            for idx_angle in range(0, self.num_projections):
                command = []

                if self.iterate:
                    input_cut = 'cuts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                    output_cut = 'cuts{:05d}_{:04d}.mha'.format(iter, idx_angle)
                else:
                    input_cut = 'cuts{:04d}.mha'.format(idx_angle)
                    output_cut = 'cuts{:04d}.mha'.format(idx_angle)

                command.append('{}/fmpctPencilBeamWEPLCorrector'.format(self.fmpct_dir))
                command.append('-i')
                command.append('{}/{}'.format(self.input_dir, input_cut))
                command.append('-o')
                command.append('{}/{}'.format(self.output_dir, output_cut))
                command.append('--weplcorrection')
                command.append(self.weplcorrection)
                command.append('--threshold')
                command.append('{}'.format(self.threshold))
                command.append('--interval')
                command.append('{}'.format(self.interval))
                command.append('--beams')
                command.append('{:d}'.format(self.beams))
                if self.relax:
                    command.append('--relax')
                if self.cut is not None:
                    command.append('--cut')
                    command.append('{:f},{:f}'.format(self.cut[0], self.cut[1]))

                self.command_queue.append(command)

        self._write_config()
        self._initialized()

    def _parse_config(self):
        BaseWorker._parse_config(self)

        self.input_dir = self._get_output_dir('cuts')
        self.output_dir = self._get_output_dir()

        self.weplcorrection = self._get_config_value(self.name, 'weplcorrection')
        self.threshold = self._get_config_value(self.name, 'threshold')
        self.interval = self._get_config_value(self.name, 'interval')
        self.beams = self._get_config_value(self.name, 'beams')
        self.relax = self._get_config_value(self.name, 'relax')
        self.iterate = self._get_config_value(self.name, 'iterate')
        self.iterate_interval = self._get_config_value(self.name, 'iterate_interval')
        self.skip_execution = self._get_config_value(self.name, 'skip_execution')
        self.cut = self._get_config_value(self.name, 'cut')

    def _validate_config(self):
        BaseWorker._validate_config(self)

        assert os.path.isfile(self.weplcorrection)
        assert ispositivenumber(self.threshold)
        assert ispositivenumber(self.interval)
        assert ispositiveint(self.beams)
        assert isbool(self.relax)
        assert isnumericlist(self.iterate_interval, minlength=3)
        for i in range(3):
            assert isint(self.iterate_interval[i])
        assert isbool(self.skip_execution)
        assert self.cut is None or isnumericlist(self.cut, minlength=2)
