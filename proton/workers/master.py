"""
proton.workers.master
Master worker that initiates all other workers
"""

import proton.util
from proton.util import *
from proton.workers.preprocess import Preprocess
from proton.workers.pairs import Pairs
from proton.workers.cuts import Cuts
from proton.workers.pbcorrection import PBCorrection
from proton.workers.bins import Bins
from proton.workers.reco import Reco
from proton.workers.copydata import CopyData
from proton.workers.view import View
from proton.util.exceptions import ConfigError


class Master:
    def __init__(self, run_config, system_config, verbose, interactive):
        self.workers = {}
        self._run_config = run_config
        self._system_config = system_config
        self.verbose = verbose
        self.interactive = interactive
        self.last_job_id = -1

        self._manipulate_config()

        if 'preprocess' in self._run_config and self._run_config['preprocess']['enabled']:
            self._setup_worker('preprocess', Preprocess)
        if 'pairs' in self._run_config and self._run_config['pairs']['enabled']:
            self._setup_worker('pairs', Pairs)
        if 'cuts' in self._run_config and self._run_config['cuts']['enabled']:
            self._setup_worker('cuts', Cuts)
        if 'pbcorrection' in self._run_config and self._run_config['pbcorrection']['enabled']:
            self._setup_worker('pbcorrection', PBCorrection)
        if 'bins' in self._run_config and self._run_config['bins']['enabled']:
            self._setup_worker('bins', Bins)
        if 'reco' in self._run_config and self._run_config['reco']['enabled']:
            self._setup_worker('reco', Reco)
        if 'view' in self._system_config and self._system_config['view']['enabled'] and self._system_config['system']['machine'] == 'local':
            self._setup_worker('view', View)
        if 'copydata' in self._system_config and self._system_config['copydata']['enabled'] and self._system_config['system']['machine'] == 'local':
            self._setup_worker('copydata', CopyData)

    def _setup_worker(self, name, cls):
        """
        Sets up a single worker adding it to the `workers` dict.

        Args:
            name (str): Name of the worker
            cls (type): Class of worker
        """
        merged_config = {**self._system_config, **self._run_config}  # merge, overwriting from run_config
        proton.util.validate_config(merged_config)
        self.workers[name] = cls(merged_config, self, self._system_config['worker_defaults'])

    def _manipulate_config(self):
        # interactively choose modules
        if self.interactive:
            print('Please choose which modules to enable. Type either y for enabling or n for disabling.')

            for worker in ['preprocess', 'pairs', 'cuts', 'pbcorrection', 'bins', 'reco', 'copydata', 'view']:
                answer = parseynanswer('Enable module <{}>?'.format(worker))
                try:
                    self._run_config[worker]['enabled'] = answer
                except KeyError:
                    pass
                try:
                    self._system_config[worker]['enabled'] = answer
                except KeyError:
                    pass

        # Generate project name
        self._run_config['scan']['project_name'] = self._run_config['scan']['project'] + '-' \
                                                   + self._run_config['scan']['phantom'] + '@' \
                                                   + str(self._run_config['scan']['energy']) + 'MeV-' \
                                                   + self._run_config['scan']['dataset']
        # Generate output directory
        if self._system_config['system']['output_location'] == 'server':
            self._system_config['system']['output_dir'] = self._system_config['system']['output_dir_server']
        elif self._system_config['system']['output_location'] == 'local':
            self._system_config['system']['output_dir'] = self._system_config['system']['output_dir_local']
        # Make enabled projects available to all workers
        enabled_workers = []
        for worker in ['preprocess', 'pairs', 'cuts', 'pbcorrection', 'bins', 'reco', 'copydata', 'view']:
            try:
                if self._run_config[worker]['enabled']:
                    enabled_workers.append(worker)
            except KeyError:
                pass
            try:
                if self._system_config[worker]['enabled']:
                    enabled_workers.append(worker)
            except KeyError:
                pass
        self._run_config['scan']['enabled_workers'] = enabled_workers

    def run(self):
        for key, worker in self.workers.items():
            worker.run(self.verbose)
