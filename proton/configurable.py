"""
acquire.configurable
"""
import copy


class Configurable(object):
    """
    Mixin class for objects that should be configured by a dictionary, with
    optional defaults.
    """
    DEFAULTS = {}

    def __init__(self, config, new_defaults=None):
        """
        :param config: configuration dict for the object.
        :param new_defaults: configuration dict for defaults that will take
                             precedence when values are missing from `config`.
        """
        self._config = config
        self._defaults = self._override_defaults(new_defaults)
        self._parse_config()
        self._validate_config()

    def _override_defaults(self, new_defaults=None):
        """
        Builds a defaults dictionary based on the class defaults first,
        overridden with the values in new_defaults.
        """
        defaults = copy.deepcopy(self.DEFAULTS)
        if new_defaults is not None:
            defaults.update(new_defaults)
        return defaults

    def _get_config_value(self, worker, name):
        """
        Gets the configuration value for the setting specified by name, falling
        back to the value in self.defaults if the setting is missing from self.config.
        """
        return self._config[worker].get(name, self._defaults.get(name))

    def _parse_config(self):
        """
        Pulls out the values from the config dict into class attributes, using
        defaults when values are missing from the config dict.
        """
        raise NotImplementedError

    def _validate_config(self):
        """
        Checks the parsed class configuration for errors, raising exceptions as
        necessary.
        """
        raise NotImplementedError
