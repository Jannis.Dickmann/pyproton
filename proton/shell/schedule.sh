#!/bin/bash

shelldir=${1}
num_jobs=${2}
partition=${3}
memory=${4}
outputpath=${5}
projectname=${6}
lastjobid=${7}
mail=${8}
max_jobs=${9}

if [[ "${partition}" = ls-parodi ]]; then
	export SLURM_CONF=/slurm_config/gar-sv-qmaster2/etc/slurm.conf
else
	export SLURM_CONF=/slurm_config/gar-sv-qmast18/etc/slurm.conf
fi

sed "s:{{projectname}}:${projectname}:
	   s:{{outputpath}}:${outputpath}:
	   s:{{partition}}:${partition}:
	   s:{{memory}}:${memory}:
	   s:{{mail}}:${mail}:
	   s:{{user}}:${USER}:" < "${shelldir}/job.sh" > "${outputpath}/cluster/${projectname}.sh"

# Make cluster script executable
chmod 777 "${outputpath}/cluster/${projectname}.sh"

# Schedule cluster job array
if [[ ${lastjobid} -ge 0 ]]; then
    jobid=$(sbatch --array=0-${num_jobs}%${max_jobs} --dependency=afterok:${lastjobid} --kill-on-invalid-dep=yes -N1 ${outputpath}/cluster/${projectname}.sh)
else
    jobid=$(sbatch --array=0-${num_jobs}%${max_jobs} -N1 ${outputpath}/cluster/${projectname}.sh)
fi

echo "$jobid"
