#!/bin/bash

########################################################################
# Cluster Settings

#SBATCH --job-name='{{projectname}}'
#SBATCH --output={{outputpath}}/cluster/{{projectname}}_%a.log
#SBATCH --error={{outputpath}}/cluster/{{projectname}}_%a.err
#SBATCH --mem={{memory}}
#SBATCH --partition={{partition}}
#SBATCH --mail-type={{mail}}
#SBATCH --mail-user={{user}}@physik.uni-muenchen.de
##SBATCH --exclude=gar-ws-med[34,35,46]

echo "Job number ${SLURM_ARRAY_TASK_ID}"

# Load Modules
source /etc/profile.d/modules.sh
module unload root
module load root/6.18.04
module unload geant4
module load geant4/10.05.p01

## Run job list
{{outputpath}}/cluster/{{projectname}}-${SLURM_ARRAY_TASK_ID}.sh
