#!/bin/bash

read -n1 -r -p "The installation will only work correctly when executed from the pyproton main directory, i.e. the directory that contains pyproton.py. Press enter to contiue or CTRL+C to cancel." key

echo ""
echo "#########################################################"
echo "Creating conda environment..."
echo "#########################################################"
echo ""

source /etc/profile.d/modules.sh
module load spack

module unload python
module load python/3.12-2024.10

conda env create -f pyproton.env.yml

echo "Activating venv..."

#conda activate pyproton

#echo "Successfully activated pyproton venv"

#pip install uproot

#echo "Deactivating venv"

#conda deactivate


echo ""
echo "#########################################################"
echo "Creating runscript..."
echo "#########################################################"
echo ""

sed "s:{{pwd}}:$PWD:" < pyproton.template.sh > pyproton.sh
chmod +x pyproton.sh

echo ""
echo "#########################################################"
echo "Modifying ~/.bashrc..."
echo "#########################################################"
echo ""

echo "" >> ~/.bashrc
echo "# alias to allow easy execution of pyproton.py" >> ~/.bashrc
echo "alias pyproton=\"$PWD/pyproton.sh\"" >> ~/.bashrc
echo "alias pyprotonscreen=\"$PWD/pyproton_screen.sh\"" >> ~/.bashrc
echo "alias medpyproton=pyprotonscreen" >> ~/.bashrc

source ~/.bashrc
