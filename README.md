# PyProton
A wrapper script for preprocessing and reconstruction of proton CT data.

## Requirements
The installation requires Anaconda3 for python package management (or any other package manager). Furthermore the following tools need to be compiled prior to running a reconstruction.
- Preprocessing code from the pCT-collaboration
- Reconstruction code from CREATIS
- ReadConvert code from LMU

## Installation
- Clone the repository to a folder of your preference using `git clone git@gitlab.physik.uni-muenchen.de:Jannis.Dickmann/pyproton.git`.
- Enter the directory `pyproton` that was created during the first operation and run `source install.sh`.
- Once the install script is finished, it should have created a new Anaconda environment called `pyproton` and modified your `~/.bashrc` such that you can run PyProton from any location using an alias.
- You can test that the installation ran successfully by invoking `pyproton --help`.
- Copy the file `system-config.sample.json` with the name `system-config.json` and open it. Modify all directories to point to the required software packages.
- Make sure that you change the last line of `pyproton.sh` so that it points to your `pyproton.py`

## Usage
- Use `cd` to go to the directory where your data is stored (either simulated or experimental data).
- Invoke `pyproton create` and input all required information (leave blank and press enter for default values). This will create a `Project.json` file (name according to what was entered).
- Open the newly created file and modify remaining settings. In particular, every reconstruction step can be enabled or disabled here.
- Invoke `pyproton Project.json` with the correct name for `Project.json` to perform the reconstruction.
