#! /bin/bash
source /etc/profile.d/modules.sh
module load spack
module unload python
module load  python/3.12-2024.10
#module unload geant4 &> /dev/null
#module load geant4 &> /dev/null
#module unload root &> /dev/null
#module load root/6.24.02 &> /dev/null
module unload root
module load root/6.32.02
#module load gcc/11.2.0

source activate pyproton

/project/med3/PROTONCT/G.Dedes/source/pyproton/pyproton.py "$@"
