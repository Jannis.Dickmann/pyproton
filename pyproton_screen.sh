#! /bin/bash

server=gar-ws-med${1}
fname=$(basename "${2}")
extension=$(echo $fname | cut -d '.' -f 2)
screenname="$(date +"%Y%m%d-%H%M%S")_${fname}"
currentdir=$PWD

if [[ "${extension}" == "json" ]]; then
  ssh ${USER}@gar-ws-med${1} << EOF
screen -dmS $screenname
screen -S $screenname -X stuff $"cd \"${currentdir}\"\n"
screen -S $screenname -X stuff $"pyproton \"${@:2}\"\n"
EOF

  echo "Connected to $server and started pyproton as deamon..."
else
  echo "Second argument must be a JSON configuration file. Quitting..."
fi