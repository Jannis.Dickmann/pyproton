#!/usr/bin/env python3

"""
PyProton is an application to run the distance-driven FDK reconstruction for proton computed tomography.

Usage:
    pyproton.py create
    pyproton.py config
    pyproton.py clean [options]
    pyproton.py [options] <run-config>

Run configuration will be read from <run-config>.
In create mode a run config will be created.

Options:
    -h, --help                       Show this screen
    -v, --verbose                    Verbose execution of jobs (optional)
    -c, --consecutive                Execute commands consecutively instead of launching in batches
    -i, --interactive                Choose reconstruction modules interactively
        --log-level <level>          Log verbosity level (optional)
    -l, --log <file>                 Log file (optional)
        --system-config <config>     Path to system config file (optional)
    -o, --output-location <loc>      Store files locally (local) or on server (server), overwriting defaults (optional)
    -p, --project <projectname>      Overwrite project name from scan config to be <projectname> (optional)
    -t, --test                       Prepend project name with "Test-", avoiding to overwrite existing data (optional)
"""

# ##############################################################################
#  pyproton                                                                    #
#                                                                              #
#  Jannis Dickmann                                                             #
#  Department of Medical Physics                                               #
#  Ludwig-Maximilians-Universitaet Muenchen                                    #
# ##############################################################################

import docopt
import proton.util
import os, time
import shutil
from proton.workers.master import Master
from proton.util.createrun import CreateRun
from subprocess import Popen


def main(args):
    verbose_execution = args['--verbose']
    interactive_execution = args['--interactive']
    log_file = args['--log']
    log_level = args['--log-level']

    if log_file is None:
        log_file = 'pyproton.log'

    # Treat the log level as case-insensitive:
    if log_level is not None:
        log_level = log_level.upper()

    # Set up the logger:
    logger = proton.util.get_logger('proton', log_level=log_level, filename=log_file)
    logger.info('Application start')
    logger.debug('Started with arguments:\n%s', args)

    run_config_filename = args['<run-config>']
    system_config_filename = args['--system-config']
    if system_config_filename is None:
        system_config_filename = os.path.join(os.path.dirname(__file__), 'system-config.json')
    system_config = proton.util.parse_config(system_config_filename, 'system')

    if args['<run-config>'] is not None:
        # load configs
        run_config = proton.util.parse_config(run_config_filename, 'run')

        # output location
        if args['--output-location'] is not None:
            system_config['system']['output_location'] = args['--output-location']

        # project name
        if args['--project'] is not None:
            run_config['scan']['project'] = args['--project']
        if args['--test']:
            run_config['scan']['project'] = 'Test-' + run_config['scan']['project']

        # multithreaded option
        if args['--consecutive']:
            system_config['system']['multithreaded_global'] = False

        # Run master worker
        master = proton.workers.master.Master(run_config, system_config, verbose_execution, interactive_execution)
        time_started = time.time()
        logger.info('Starting execution of submitted jobs.')
        master.run()
        time_elapsed = time.time() - time_started
        time_elapsed_str = '{0:.1f} min'.format(time_elapsed/60)
        logger.info('Time elapsed is %s.', time_elapsed_str)
    elif args['create']:
        # Create config file
        run_config = proton.util.parse_config(os.path.join(os.path.dirname(__file__), 'run.template.json'), 'run')
        run = CreateRun(run_config, system_config, logger.level)
        run.write()
    elif args['config']:
        Popen([system_config['system']['editor'], system_config_filename])
        logger.info('Opened system configuration at <{}> with gedit.'.format(system_config_filename))
    elif args['clean']:
        tmp_dir = system_config['system']['output_dir_local']
        logger.info('Removing local output directory <{}>.'.format(tmp_dir))
        if proton.util.parseynanswer('Continue?'):
            logger.info('Removing...')
            shutil.rmtree(tmp_dir)
            os.makedirs(tmp_dir)
        else:
            logger.info('User cancelled removing of local output directory.')
    else:
        logger.warning('Please either provide a run configuration file using "pyproton.py <run-config>" or invoke ' \
                    + '"pyproton.py create" within your data directoy.')
    logger.info('Goodbye :-)')


if __name__ == '__main__':
    # Parse the command line arguments:
    args = docopt.docopt(__doc__)
    main(args)
